/* Copyright 2020 Emanuele Sorce
 * 
 * This file is part of Parabola and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick 2.9
import QtQuick.Layouts 1.3

Page {
	
	header: PageHeader {
		id: header
		title: i18n.tr('Parabola')
		
		leadingActionBar {
			actions: [
				Action {
					iconName: "back"
					text: "back"
					onTriggered: pageStack.pop()
				}
			]
		}
	}
	
	Flickable {
		id: flickable
		anchors.fill: parent
		contentHeight:  layout.height + units.dp(80)
		contentWidth: parent.width
		
		Column {
			id: layout

			spacing: units.dp(35)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.right: parent.right
			width: parent.width
			
			Item {
				height: units.dp(30)
				width: units.dp(5)
			}
			
			Image {
				anchors.horizontalCenter: parent.horizontalCenter
				height: width
				width: Math.min(parent.width/2, parent.height/3)
				source: Qt.resolvedUrl("qrc:///assets/logo.svg")
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				font.pixelSize: units.dp(25)
				font.bold: true
				horizontalAlignment: Text.AlignHCenter
				wrapMode: Text.WordWrap
				text: i18n.tr("Parabola")
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("Parabola is an open source app that pipes microphone input into audio output turning your device into a microphone.");
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("This app is licensed with GNU GPLv3 and uses some Suru icons in its logo")
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("Feedback or any help is welcome, feel free to contact me!")
			}
			
			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: i18n.tr("❤Donate❤")
				color: Theme.palette.normal.positive
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://paypal.me/emanuele42");
			}
			
			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: i18n.tr("Fork me on GitLab")
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://gitlab.com/TronFortyTwo/parabola");
			}
			
			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: i18n.tr("Report bug or feature request")
				color: Theme.palette.normal.positive
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://gitlab.com/TronFortyTwo/parabola");
			}
			
			Button {
				anchors.horizontalCenter: parent.horizontalCenter
				text: i18n.tr("See License (GNU GPL v3)")
				width: parent.width * 4 / 5
				onClicked: Qt.openUrlExternally("https://gitlab.com/TronFortyTwo/parabola/-/blob/master/LICENSE");
			}
			
			Label {
				anchors.horizontalCenter: parent.horizontalCenter
				width: parent.width * 4 / 5
				wrapMode: Text.WordWrap
				horizontalAlignment: Text.AlignHCenter
				text: "Copyright (C) 2021 Emanuele Sorce (emanuele.sorce@hotmail.com)"
			}
		}
	}
}
